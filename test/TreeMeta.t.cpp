#include "gtest/gtest.h"

#include "TreeMeta.hpp"

void validate_meta(const cov::implementation::TreeMeta &meta,
                   std::vector<cov::implementation::DepthMeta> expected) {
	EXPECT_EQ(meta.tree_height(), expected.size());

	for (std::size_t i = 0; i < meta.tree_height(); ++i) {
		EXPECT_EQ(meta[i], expected[i]);
	}
}

TEST(TreeMeta, TreeMetaComputationTest) {
	cov::implementation::TreeMeta meta2(2);
	const std::vector<cov::implementation::DepthMeta> height_two_meta = {
	    {0, 0, 0}, {1, 1, 0}, {1, 3, 0}};

	validate_meta(meta2, height_two_meta);

	cov::implementation::TreeMeta meta3(3);
	const std::vector<cov::implementation::DepthMeta> height_three_meta = {
	    {0, 0, 0}, {1, 1, 0}, {3, 3, 0}, {1, 1, 2}

	};

	validate_meta(meta3, height_three_meta);

	cov::implementation::TreeMeta meta4(4);
	const std::vector<cov::implementation::DepthMeta> height_four_meta = {
	    {0, 0, 0}, {1, 1, 0}, {1, 3, 0}, {3, 7, 0}, {1, 1, 3},
	};

	validate_meta(meta4, height_four_meta);

	cov::implementation::TreeMeta meta5(5);
	const std::vector<cov::implementation::DepthMeta> height_five_meta = {
	    {0, 0, 0}, {1, 1, 0}, {1, 3, 0}, {7, 7, 0}, {1, 1, 3}, {1, 3, 3}};

	validate_meta(meta5, height_five_meta);
}
