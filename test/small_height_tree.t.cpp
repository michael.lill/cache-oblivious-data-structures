#include <numeric>
#include <random>

#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "small_height_tree.hpp"

using namespace ::testing;

TEST(small_height_tree, insertions_test) {
	cov::small_height_tree<int> tree;
	const auto &[iter1, inserted1] = tree.insert(5);
	EXPECT_TRUE(inserted1);
	EXPECT_EQ(*iter1, 5);

	const auto &[iter2, inserted2] = tree.insert(1);
	EXPECT_TRUE(inserted2);
	EXPECT_EQ(*iter2, 1);

	const auto &[iter3, inserted3] = tree.insert(6);
	EXPECT_TRUE(inserted3);
	EXPECT_EQ(*iter3, 6);

	std::vector<int> traversal_result;
	tree.inorder({1, 1}, [&traversal_result](const std::optional<int> &node) {
		EXPECT_TRUE(node.has_value());
		traversal_result.push_back(node.value());
	});

	EXPECT_THAT(traversal_result, ContainerEq(std::vector<int>{1, 5, 6}));
}

TEST(small_height_tree, many_insertions_test) {
	cov::small_height_tree<int> tree;
	for (auto i = 1; i <= 100; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	std::vector<int> inorder;
	for (const auto &node : tree) {
		EXPECT_TRUE(node.has_value());
		inorder.push_back(node.value());
	}

	std::vector<int> expected;
	std::generate_n(std::back_inserter(expected), 100,
	                [&]() { return expected.size() + 1; });

	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(small_height_tree, double_insertion_test) {
	cov::small_height_tree<int> tree(3);
	tree.insert(1);
	const auto [iter, inserted] = tree.insert(1);

	EXPECT_FALSE(inserted);
	EXPECT_EQ(*iter, 1);
}

TEST(small_height_tree, InsertionWithRebalancingTest) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 1; i <= 4; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	// Trigger the rebalancing, but not the resizing.
	const auto [iter, inserted] = tree.insert(5);
	EXPECT_TRUE(inserted);
	EXPECT_EQ(*iter, 5);
}

TEST(small_height_tree, iterator_traversal_test) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 1; i <= 4; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	std::vector<int> inorder;
	for (auto iter = tree.find(2); iter != tree.end(); ++iter) {
		EXPECT_TRUE(iter->has_value());
		inorder.push_back(iter->value());
	}

	EXPECT_THAT(inorder, ContainerEq(std::vector<int>{2, 3, 4}));
}

TEST(small_height_tree, iterator_mutation_test) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 1; i <= 4; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);

		*iter = i - 1;
	}

	std::vector<int> inorder;
	for (const auto &node : tree) {
		EXPECT_TRUE(node.has_value());
		inorder.push_back(node.value());
	}

	EXPECT_THAT(inorder, ContainerEq(std::vector<int>{0, 1, 2, 3}));
}

TEST(small_height_tree, insertion_with_rebalancing_and_resizing_test) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 1; i <= 3; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	// Resizal should be in order to maintain the density invariant of the root.
	const auto [iter, inserted] = tree.insert(4);
	EXPECT_TRUE(inserted);
	EXPECT_EQ(*iter, 4);

	EXPECT_EQ(tree.height(), 4);
}

TEST(small_height_tree, backwards_iteration_test) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 0; i <= 20; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter = tree.find(5);
	std::vector<int> ordered{iter->value()};

	do {
		--iter;
		ordered.push_back(iter->value());
	} while (iter != tree.begin());

	EXPECT_THAT(ordered, ContainerEq(std::vector<int>{5, 4, 3, 2, 1, 0}));
}

TEST(small_height_tree, erase_test) {
	cov::small_height_tree<int> tree(3);
	for (auto i = 0; i <= 10; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter = tree.find(5);
	EXPECT_EQ(*iter, 5);

	auto next_iter = tree.erase(iter);
	EXPECT_EQ(*next_iter, 6);

	std::vector<int> ordered;
	for (const auto &x : tree) {
		ordered.push_back(*x);
	}

	std::vector<int> expected(11);
	std::iota(expected.begin(), expected.end(), 0);
	expected.erase(std::find(expected.begin(), expected.end(), 5));

	EXPECT_THAT(ordered, ContainerEq(expected));
}

TEST(small_height_tree, iterator_swap_test) {
	cov::small_height_tree<int> tree(3);

	for (auto i = 0; i < 10; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	auto iter1 = tree.find(5);
	auto iter2 = tree.find(6);
	std::swap(iter1, iter2);

	std::vector<int> ordered;
	for (const auto &x : tree) {
		ordered.push_back(*x);
	}

	std::vector<int> expected(10);
	std::iota(expected.begin(), expected.end(), 0);

	EXPECT_THAT(ordered, ContainerEq(expected));
}

TEST(small_height_tree, load_test) {
	cov::small_height_tree<int> tree;

	const auto insertions = 1500;
	for (auto i = 0; i <= insertions; ++i) {
		const auto [iter, inserted] = tree.insert(i);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, i);
	}

	for (auto i = 700; i <= 1000; ++i) {
		auto iter = tree.find(i);

		auto next_iter = tree.erase(iter);
		EXPECT_EQ(*next_iter, i + 1);
	}

	std::vector<int> inorder;
	for (const auto &v : tree) {
		inorder.push_back(*v);
	}

	std::vector<int> expected(700);
	std::iota(expected.begin(), expected.end(), 0);

	std::vector<int> upper_half(500);
	std::iota(upper_half.begin(), upper_half.end(), 1001);

	expected.insert(expected.end(), upper_half.begin(), upper_half.end());
	EXPECT_THAT(inorder, ContainerEq(expected));
}

TEST(small_height_tree, random_insertion_test) {
	const uint64_t seed = 2212383;
	std::mt19937 rng(seed);
	std::uniform_int_distribution<int> dist{};

	const auto elements = 500;
	cov::small_height_tree<int> tree;
	std::set<int> std_set;

	for (auto i = 0; i < elements; ++i) {
		const auto v = dist(rng);
		std_set.insert(v);
		const auto [iter, inserted] = tree.insert(v);
		EXPECT_TRUE(inserted);
		EXPECT_EQ(*iter, v);
	}

	std::vector<int> inorder;
	for (const auto &node : tree) {
		EXPECT_TRUE(node.has_value());
		inorder.push_back(node.value());
	}

	std::vector<int> expected(std_set.begin(), std_set.end());

	EXPECT_THAT(inorder, ContainerEq(expected));
}
