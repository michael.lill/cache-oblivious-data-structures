#pragma once

#include <algorithm>
#include <cmath>
#include <iterator>
#include <memory>
#include <vector>

#include "TreeMeta.hpp"
#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "util.hpp"

namespace cov {

template <typename T>
class static_veb_tree {
public:
	static constexpr std::size_t npos = -1;

	explicit static_veb_tree();

	template <typename InputIt>
	explicit static_veb_tree(InputIt first, InputIt last);

	[[nodiscard]] std::size_t size() const;
	[[nodiscard]] std::size_t find(const T &value) const;

	const T &operator[](std::size_t idx) const;

private:
	using static_index = std::pair<std::size_t, std::size_t>;

	[[nodiscard]] inline std::size_t veb_index(
	    std::size_t bfs_index,
	    const std::vector<static_index> &path) const;
	[[nodiscard]] inline static_index left(static_index index,
	                                       const std::vector<static_index> &path) const;
	[[nodiscard]] inline static_index right(static_index index,
	                                        const std::vector<static_index> &path) const;

	template <typename InputIt>
	void populate(InputIt iter);

	template <typename InputIt>
	InputIt populate_impl(InputIt iter,
	                      static_index index,
	                      std::vector<static_index> &path);

private:
	std::size_t size_ = 0;
	std::size_t depth_ = 0;
	std::unique_ptr<T[]> data_;
	implementation::TreeMeta meta_ = implementation::TreeMeta(0);
};

template <typename T>
inline static_veb_tree<T>::static_veb_tree() = default;

template <typename T>
template <typename InputIt>
inline static_veb_tree<T>::static_veb_tree(InputIt first, InputIt last)
    : size_(std::distance(first, last))
    , depth_(std::floor(std::log2(size_)))
    , data_(std::make_unique<T[]>(size_))
    , meta_(depth_) {
	if (std::is_sorted(first, last)) {
		populate(first);

		return;
	}

	std::vector<T> copy_of_range(first, last);
	std::sort(copy_of_range.begin(), copy_of_range.end());

	populate(copy_of_range.begin());
}

template <typename T>
inline std::size_t static_veb_tree<T>::size() const {
	return size_;
}

template <typename T>
inline auto static_veb_tree<T>::find(const T &value) const -> std::size_t {
	std::vector<static_index> path;
	path.reserve(depth_ + 1);

	std::size_t current_depth = 0;
	static_index current = {0, 1};
	while (current.first < size_ and current_depth <= depth_) {
		path.push_back(current);

		const auto &node = data_[current.first];
		if (node == value) {
			return current.first;
		}

		if (current_depth < depth_) {
			current = value < node ? left(current, path) : right(current, path);
		}

		current_depth++;
	}

	return npos;
}

template <typename T>
inline auto static_veb_tree<T>::operator[](std::size_t idx) const -> const T & {
	return data_[idx];
}

template <typename T>
inline std::size_t static_veb_tree<T>::veb_index(
    std::size_t bfs_index,
    const std::vector<static_index> &path) const {
	DEBUG_ASSERT(path.size() == static_cast<std::size_t>(std::log2(bfs_index)),
	             assert_config{}, "The size of the path does not match the depth");

	const auto depth = path.size();
	const auto &meta = meta_[depth];
	const auto current_subtree = bfs_index & meta.top_tree_size;

	return path[meta.top_tree_root_depth].first + meta.top_tree_size +
	       current_subtree * meta.bottom_tree_size;
}

template <typename T>
inline auto static_veb_tree<T>::left(static_index index,
                                     const std::vector<static_index> &path) const
    -> static_index {
	const auto left_bfs = index.second << 1U;
	return {veb_index(left_bfs, path), left_bfs};
}

template <typename T>
inline auto static_veb_tree<T>::right(static_index index,
                                      const std::vector<static_index> &path) const
    -> static_index {
	const auto right_bfs = (index.second << 1U) + 1;
	return {veb_index(right_bfs, path), right_bfs};
}

template <typename T>
template <typename InputIt>
inline void static_veb_tree<T>::populate(InputIt iter) {
	std::vector<static_index> path;
	path.reserve(depth_ + 1);

	populate_impl(iter, {0, 1}, path);
}

template <typename T>
template <typename InputIt>
inline InputIt static_veb_tree<T>::populate_impl(InputIt iter,
                                                 static_index index,
                                                 std::vector<static_index> &path) {
	if (index.first >= size_ or path.size() > depth_) {
		return iter;
	} else if (path.size() == depth_) {
		data_[index.first] = *iter;
		return ++iter;
	}

	path.push_back(index);

	const auto left_child = left(index, path);
	iter = populate_impl(iter, left_child, path);

	data_[index.first] = *iter;
	++iter;

	const auto right_child = right(index, path);
	iter = populate_impl(iter, right_child, path);

	path.pop_back();

	return iter;
}

} // namespace cov
