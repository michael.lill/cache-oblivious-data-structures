#pragma once

#include "bfs_index.hpp"
#include "cov_tree.hpp"
#include "packed_memory_array.hpp"

namespace cov {

template <typename T>
using hybrid_tree =
    cov_tree<T,
             packed_memory_array<T>,
             implementation::bfs_index<T, typename packed_memory_array<T>::iterator>>;
}
