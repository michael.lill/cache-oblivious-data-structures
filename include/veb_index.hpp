#pragma once

#include <algorithm>
#include <any>
#include <cassert>
#include <cmath>
#include <functional>
#include <iterator>
#include <optional>
#include <type_traits>
#include <variant>
#include <vector>

#include "TreeMeta.hpp"
#include "assert_config.hpp"
#include "debug_assert.hpp"
#include "util.hpp"

namespace cov::implementation {

template <typename T, typename Iter>
class node;

template <typename T, typename Iter>
class veb_index {
public:
	explicit veb_index(Iter first, Iter last);

	Iter find(const T &value) const;

	template <typename Pred>
	Iter update(Iter first, Iter last, Pred &&pred);

	[[nodiscard]] inline const std::vector<node<T, Iter>> &data() const;
	[[nodiscard]] inline std::size_t size() const;

private:
	void populate_index();
	std::optional<T> populate_index_implementation(std::size_t root,
	                                               std::vector<std::size_t> &path);

	[[nodiscard]] std::size_t inline compute_veb_index(
	    std::size_t bfs_index,
	    const std::vector<std::size_t> &path) const;

private:
	Iter first_;
	Iter last_;

	std::size_t tree_size_;
	std::size_t max_depth_;
	std::size_t leaves_offset_;

	TreeMeta meta_;

	std::vector<node<T, Iter>> tree_;
	std::vector<std::size_t> leaf_index_;
};

template <typename T, typename Iter>
class node {
public:
	using value_type = std::variant<std::optional<T>, Iter>;

private:
	value_type value_;
	std::size_t parent_index_;
	std::size_t left_child_index_;

public:
	node() = default;

	node(value_type value, std::size_t parent_index, std::size_t left_child_index = 0)
	    : value_(std::move(value))
	    , parent_index_(parent_index)
	    , left_child_index_(left_child_index){}

	          [[nodiscard]] std::size_t left_child_index() const {
		return left_child_index_;
	}

	[[nodiscard]] std::size_t parent_index() const { return parent_index_; }

	    [[nodiscard]] std::optional<T> value() const {
		return std::visit(
		    [](auto &&v) -> std::optional<T> {
			    using E = std::decay_t<decltype(v)>;
			    if constexpr (std::is_same_v<E, Iter>) {
				    return *v;
			    } else {
				    return v;
			    }
		    },
		    value_);
	}

	[[nodiscard]] const value_type &raw_value() const { return value_; }

	template <typename E>
	void set_value(const E &value) {
		value_ = value;
	}

	void set_value(const value_type &value) {
		if (std::holds_alternative<Iter>(value)) {
			value_ = *std::get<Iter>(value);
		} else {
			value_ = std::get<std::optional<T>>(value);
		}
	}

	constexpr bool operator==(const node &other) const {
		if (parent_index_ != other.parent_index_) {
			return false;
		}

		/* Special case the compare the values pointed to */
		if (std::holds_alternative<Iter>(value_) and
		    std::holds_alternative<Iter>(other.value_)) {
			return *(std::get<Iter>(value_)) == *(std::get<Iter>(other.value_));
		}

		return value_ == other.value_;
	}

	constexpr bool operator<(const node &other) const { return value() < other.value(); }
};

template <typename T, typename Iter>
std::ostream &operator<<(std::ostream &os, const node<T, Iter> &node) {
	if (std::holds_alternative<Iter>(node.raw_value())) {
		os << "[Iterator to: " << *(std::get<Iter>(node.raw_value())) << "]";
	} else {
		os << "[Optional: " << std::get<std::optional<T>>(node.raw_value()) << "]";
	}

	return os;
}

template <typename T, typename Iter>
inline veb_index<T, Iter>::veb_index(Iter first, Iter last)
    : first_(first)
    , last_(last)
    , tree_size_((2 * std::distance(first_, last_)) - 1)
    , max_depth_(std::log2(tree_size_))
    , leaves_offset_(std::pow(2, max_depth_))
    , meta_(max_depth_)
    , tree_(tree_size_)
    , leaf_index_() {
	populate_index();
}

template <typename T, typename Iter>
inline Iter veb_index<T, Iter>::find(const T &value) const {
	std::size_t bfs_index = 1;

	std::vector<std::size_t> path;
	path.reserve(max_depth_ + 1);
	path.push_back(0);

	std::size_t depth = 0;
	while (depth < max_depth_) {
		DEBUG_ASSERT(
		    std::holds_alternative<std::optional<T>>(tree_[path.back()].raw_value()),
		    assert_config{}, "Leaf node inspected during search");

		const auto left = compute_veb_index(bfs_index * 2, path);
		const auto right = compute_veb_index(bfs_index * 2 + 1, path);

		if (depth < max_depth_ - 1 and
		    std::get<std::optional<T>>(tree_[left].raw_value()).has_value() and
		    std::get<std::optional<T>>(tree_[right].raw_value()).has_value()) {
			DEBUG_ASSERT(std::max(tree_[left], tree_[right]) == tree_[right],
			             assert_config{}, "Left child of node has a bigger value");
		}

		bool go_left = tree_[left].value() >= value;
		bfs_index = go_left ? bfs_index * 2 : bfs_index * 2 + 1;

		++depth;
		path.push_back(compute_veb_index(bfs_index, path));
	}

	DEBUG_ASSERT(std::holds_alternative<Iter>(tree_[path.back()].raw_value()),
	             assert_config{}, "Search result is a non leaf node");

	return std::get<Iter>(tree_[path.back()].raw_value());
}

template <typename T, typename Iter>
template <typename Pred>
inline Iter veb_index<T, Iter>::update(Iter first, Iter last, Pred &&pred) {
	if (std::distance(first_, first) % 2 == 1) {
		--first;
	}

	if (std::distance(first_, last) % 2 == 1) {
		++last;
	}

	auto found = last;
	for (auto iter = first; iter != last; iter += 2) {
		/* Check if any of the two peer leaves satisfy the predcate. */
		if (found == last) {
			if (pred(*iter)) {
				found = iter;
			} else if (pred(*(iter + 1))) {
				found = iter + 1;
			}
		}

		std::optional<T> local_max = std::max(*iter, *(iter + 1));

		std::size_t bfs = leaves_offset_ + std::distance(first_, iter + 1);
		std::size_t veb = leaf_index_[std::distance(first_, iter + 1)];

		DEBUG_ASSERT(bfs % 2 == 1, assert_config{},
		             "Propagation must start from a right child");

		while (not is_left_child(bfs) and bfs != 1) {
			const auto parent = tree_[veb].parent_index();

			/* If propagating null check if the left child has a value. If it does
			 * 'pick it up' and propagate it. */
			if (local_max == std::nullopt) {
				const auto peer = tree_[parent].left_child_index();
				local_max = tree_[peer].value();
			}

			tree_[parent].set_value(local_max);

			bfs /= 2;
			veb = parent;
		}
	}

	return found;
}

template <typename T, typename Iter>
inline const std::vector<node<T, Iter>> &veb_index<T, Iter>::data() const {
	return tree_;
}

template <typename T, typename Iter>
inline std::size_t veb_index<T, Iter>::size() const {
	return std::distance(first_, last_);
}

template <typename T, typename Iter>
inline void veb_index<T, Iter>::populate_index() {
	std::vector<std::size_t> path;
	path.reserve(max_depth_ + 1);

	leaf_index_.reserve(std::distance(first_, last_));

	populate_index_implementation(1, path);
}

template <typename T, typename Iter>
inline std::optional<T> veb_index<T, Iter>::populate_index_implementation(
    std::size_t root,
    std::vector<std::size_t> &path) {
	const auto depth = path.size();
	const auto veb = compute_veb_index(root, path);

	const auto parent_index = path.empty() ? 0 : path.back();
	DEBUG_ASSERT(parent_index < tree_.size(), assert_config{},
	             "The veb index of the parent is not within bounds");

	/* If on a leaf node, update tree_ and leaf_index then bail. */
	if (depth == max_depth_) {
		Iter range_iter = first_ + (root - leaves_offset_);
		tree_[veb] = node<T, Iter>(range_iter, parent_index);
		leaf_index_.push_back(veb);

		return *range_iter;
	}

	/* Otherwise, visit the children. */
	path.push_back(veb);

	const auto left = populate_index_implementation(root * 2, path);
	const auto right = populate_index_implementation(root * 2 + 1, path);

	if (right.has_value()) {
		DEBUG_ASSERT(left < right, assert_config{}, "Max was found in the left subtree");
	}

	const auto left_veb_index = compute_veb_index(root * 2, path);

	path.pop_back();

	tree_[veb] = node<T, Iter>(std::max(left, right), parent_index, left_veb_index);

	return std::max(left, right);
}

template <typename T, typename Iter>
inline std::size_t veb_index<T, Iter>::compute_veb_index(
    std::size_t bfs_index,
    const std::vector<std::size_t> &path) const {
	if (bfs_index == 1) {
		return 0;
	}

	const auto depth = std::log2(bfs_index);
	const auto &meta = meta_[depth];
	const auto current_subtree = bfs_index & meta.top_tree_size;

	return path[meta.top_tree_root_depth] + meta.top_tree_size +
	       current_subtree * meta.bottom_tree_size;
}

} // namespace cov::implementation
