#pragma once

#include "debug_assert.hpp"

struct assert_config : debug_assert::default_handler,
                       debug_assert::set_level<ASSERTION_LEVEL> {};
