import sys
import subprocess

N = 2 ** 28 * 4
SIZE = 4

def get_experiment_size():
    try:
        output = subprocess.check_output(['free', '-b']).decode("utf-8")
        lines = output.splitlines()
        total_bytes = int(lines[1].split()[1])

        if total_bytes < N: 
            print('Using a third of available RAM memory ...')
            return total_bytes / 3
    except:
        print('Failed to detect available RAM. Using 0.5GB ...')

    return N / 2

if __name__ == '__main__':
    if len(sys.argv) > 1:
        SIZE = sys.argv[1]

    base = 10 ** (1 / 10)
    n = base 

    experiment_size = get_experiment_size()
    while n * int(SIZE) < experiment_size:
        print(int(n))
        while int(n*base) == int(n):
            n *= base
        n *= base
