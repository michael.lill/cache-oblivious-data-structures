#include <benchmark/benchmark.h>

#include <algorithm>
#include <array>
#include <iterator>

#include "static_bfs_tree.hpp"
#include "static_veb_tree.hpp"

#include "benchmark_config.hpp"

using inserted_type = std::uint32_t;
static constexpr uint64_t SEARCHES = 3 * 1000000;

static void BM_BinarySearch(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	std::vector<inserted_type> tree(ELEMENTS);
	std::generate(tree.begin(), tree.end(), [&]() { return gen.next(); });
	std::sort(tree.begin(), tree.end());

	std::vector<inserted_type> to_search(SEARCHES);
	std::generate(to_search.begin(), to_search.end(), [&]() { return gen.next(); });

	uint64_t iterations = 0;
	uint64_t sum = 0;
	static const std::size_t npos = -1;
	for (auto _ : state) {
		for (const auto& x : to_search) {
			const auto iter = std::lower_bound(tree.begin(), tree.end(), x);
			const std::size_t index = (iter == tree.end() || *iter != x) ?
			                              -1 :
			                              std::distance(tree.begin(), iter);

			sum += (index == npos) ? 0 : tree[index];
		}
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_StaticVeb(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	std::vector<inserted_type> values(ELEMENTS);
	std::generate(values.begin(), values.end(), [&]() { return gen.next(); });

	std::vector<inserted_type> to_search(SEARCHES);
	std::generate(to_search.begin(), to_search.end(), [&]() { return gen.next(); });

	uint64_t iterations = 0;
	uint64_t sum = 0;
	cov::static_veb_tree<inserted_type> tree(values.begin(), values.end());
	for (auto _ : state) {
		for (const auto& x : to_search) {
			std::size_t index = tree.find(x);
			sum += (index == cov::static_veb_tree<inserted_type>::npos) ? 0 : tree[index];
		}
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_StaticBfsBranchy(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	std::vector<inserted_type> values(ELEMENTS);
	std::generate(values.begin(), values.end(), [&]() { return gen.next(); });

	std::vector<inserted_type> to_search(SEARCHES);
	std::generate(to_search.begin(), to_search.end(), [&]() { return gen.next(); });

	uint64_t iterations = 0;
	uint64_t sum = 0;
	cov::static_bfs_tree<inserted_type, cov::find_strategy_branchy> tree(values.begin(),
	                                                                     values.end());
	for (auto _ : state) {
		for (const auto& x : to_search) {
			std::size_t index = tree.find(x);
			sum +=
			    (index ==
			     cov::static_bfs_tree<inserted_type, cov::find_strategy_branchy>::npos) ?
			        0 :
			        tree[index];
		}
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

static void BM_StaticBfsBranchFree(benchmark::State& state) {
	random_generator<inserted_type> gen(0, 2 * ELEMENTS + 2);

	std::vector<inserted_type> values(ELEMENTS);
	std::generate(values.begin(), values.end(), [&]() { return gen.next(); });

	std::vector<inserted_type> to_search(SEARCHES);
	std::generate(to_search.begin(), to_search.end(), [&]() { return gen.next(); });

	uint64_t iterations = 0;
	uint64_t sum = 0;
	cov::static_bfs_tree<inserted_type, cov::find_strategy_branch_free> tree(
	    values.begin(), values.end());
	for (auto _ : state) {
		for (const auto& x : to_search) {
			std::size_t index = tree.find(x);
			sum += (index == cov::static_bfs_tree<inserted_type,
			                                      cov::find_strategy_branch_free>::npos) ?
			           0 :
			           tree[index];
		}
		++iterations;
	}

	state.counters["sum_found"] = static_cast<int>(sum / iterations);
}

BENCHMARK(BM_BinarySearch);
BENCHMARK(BM_StaticBfsBranchy);
BENCHMARK(BM_StaticBfsBranchFree);
BENCHMARK(BM_StaticVeb);


int main(int argc, char** argv) {
	std::cout << "Running benchmarks with " << ELEMENTS << " elements" << std::endl;

	::benchmark::Initialize(&argc, argv);
	if (::benchmark::ReportUnrecognizedArguments(argc, argv)) {
		return 1;
	}

	::benchmark::RunSpecifiedBenchmarks();
}
